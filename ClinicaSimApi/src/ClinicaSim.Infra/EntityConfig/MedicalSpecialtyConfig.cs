﻿using ClinicaSim.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ClinicaSim.Infra.EntityConfig
{
    public class MedicalSpecialtyConfig : IEntityTypeConfiguration<MedicalSpecialty>
    {
        public void Configure(EntityTypeBuilder<MedicalSpecialty> builder)
        {
            builder.ToTable("MedicalSpecialty");
            builder.HasKey(x => x.Id);
            builder.Property(X => X.Description).HasMaxLength(255).IsRequired();

        }
    }
}
