﻿using ClinicaSim.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Infra.EntityConfig
{
    public class DoctorConfig : IEntityTypeConfiguration<Doctor>
    {
        public void Configure(EntityTypeBuilder<Doctor> builder)
        {
            builder.ToTable("Doctors");
            builder.HasKey(x => x.Id);
            builder.Property(X => X.Name).HasMaxLength(255).IsRequired();
            builder.Property(x => x.CPF).HasMaxLength(14);
            builder.Property(X => X.Active).IsRequired().HasDefaultValue(true);
            builder.Property(X => X.MedicalSpecialtyId);
            builder.Property(X => X.DateCreate).IsRequired();

            builder.HasOne(b => b.MedicalSpecialty)
           .WithMany(b => b.Doctors)
           .HasForeignKey(b => b.MedicalSpecialtyId);


            builder.Ignore(x => x.Notifications);

        }
    }
}
