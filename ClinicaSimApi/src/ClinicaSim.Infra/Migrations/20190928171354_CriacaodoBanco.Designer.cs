﻿// <auto-generated />
using ClinicaSim.Infra;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace ClinicaSim.Infra.Migrations
{
    [DbContext(typeof(ClinicaSimContext))]
    [Migration("20190928171354_CriacaodoBanco")]
    partial class CriacaodoBanco
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026");

            modelBuilder.Entity("ClinicaSim.Domain.Entities.Doctor", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<string>("CPF")
                        .HasMaxLength(14);

                    b.Property<DateTime>("DateCreate");

                    b.Property<int>("MedicalSpecialtyId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("MedicalSpecialtyId");

                    b.ToTable("Doctors");
                });

            modelBuilder.Entity("ClinicaSim.Domain.Entities.MedicalSpecialty", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("MedicalSpecialty");
                });

            modelBuilder.Entity("ClinicaSim.Domain.Entities.Doctor", b =>
                {
                    b.HasOne("ClinicaSim.Domain.Entities.MedicalSpecialty", "MedicalSpecialty")
                        .WithMany("Doctors")
                        .HasForeignKey("MedicalSpecialtyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
