﻿using ClinicaSim.Domain.Entities;
using ClinicaSim.Infra.EntityConfig;
using Microsoft.EntityFrameworkCore;

namespace ClinicaSim.Infra
{
    public class ClinicaSimContext : DbContext
    {
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<MedicalSpecialty> MedicalSpecialty { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(@"User ID=postgres;Password=senha123;Server=localhost;Port=5432;Database=Teste3;Integrated Security=true; Pooling=true;");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new DoctorConfig());
            builder.ApplyConfiguration(new MedicalSpecialtyConfig());
            //builder.ApplyConfiguration(new CategoryMap());

            base.OnModelCreating(builder);
        }

        /// override void seed

    }
}
