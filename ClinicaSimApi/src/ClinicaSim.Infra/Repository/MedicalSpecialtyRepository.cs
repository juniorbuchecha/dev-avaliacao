﻿using ClinicaSim.Domain.Entities;
using ClinicaSim.Domain.Interfaces.Repository;

namespace ClinicaSim.Infra.Repository
{
    public class MedicalSpecialtyRepository : Repository<MedicalSpecialty>, IMedicalSpecialtyRepository
    {
    }
}
