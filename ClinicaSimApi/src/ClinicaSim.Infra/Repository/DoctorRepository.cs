﻿using System.Collections.Generic;
using ClinicaSim.Domain.Entities;
using ClinicaSim.Domain.Interfaces.Repository;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ClinicaSim.Domain.ReadOnly;

namespace ClinicaSim.Infra.Repository
{
    public class DoctorRepository : Repository<Doctor>, IDoctorRepository
    {

        public IEnumerable<DoctorReadOnly> GetAllDoctorSpecialty()
        {
            string sql = @"SELECT d.""Id"", d.""Active"", d.""CPF"", d.""MedicalSpecialtyId"", d.""Name"", m.""Description"" as ""MedicalSpecialty""
                                FROM public.""Doctors"" AS  D
                                INNER JOIN public.""MedicalSpecialty"" m
                                ON  D.""MedicalSpecialtyId"" = m.""Id""";

            return _db.Database.GetDbConnection().Query<DoctorReadOnly>(sql).ToList();
        }

        public Doctor GetCPF(string CPF)
        {
            return _db.Doctors.Where(x => x.CPF == CPF).FirstOrDefault();
        }
    }
}
