﻿using ClinicaSim.Domain.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClinicaSim.Infra.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly ClinicaSimContext _db;
        protected readonly DbSet<TEntity> _dbSet;

        public Repository()
        {
            _db = new ClinicaSimContext(); //depois aplicar IOC
            _dbSet = _db.Set<TEntity>();
        }


        public virtual TEntity Create(TEntity obj)
        {
            _dbSet.Add(obj);
            //  SaveChanges();
            return obj;
        }

        public virtual TEntity Update(TEntity obj)
        {
            var entry = _db.Entry(obj);
            _dbSet.Attach(obj);
            entry.State = EntityState.Modified;

            return obj;
        }

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
            _db.Dispose();
        }

        public virtual TEntity Get(Guid id)
        {
            return _dbSet.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual void Delete(TEntity obj)
        {
            var entry = _db.Entry(obj);
            _dbSet.Attach(obj);
            entry.State = EntityState.Deleted;

        }

        public int SaveChanges()
        {
            return _db.SaveChanges();
        }
    }
}
