import { BaseModel } from './base.model';

export class MedicalSpecialty extends BaseModel {

    constructor(
        public id?: number,
        public description?: string
    ) {
        super();
    }

    static fromJson(jsonData: any): MedicalSpecialty {
        return Object.assign(new MedicalSpecialty(), jsonData);
    }

}


