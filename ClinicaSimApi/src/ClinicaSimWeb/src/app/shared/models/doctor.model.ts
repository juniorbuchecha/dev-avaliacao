import { BaseModel } from './base.model';

export class Doctor extends BaseModel {

    // id: string;
    // cpf: string;
    // name: string;
    active: boolean;
    //  medicalSpecialtyId: string;
    medicalSpecialty: string;

    constructor(
        public id?: string,
        public cpf?: string,
        public name?: string,
        public medicalSpecialtyId?: string
    ) {
        super();
    }

    static fromJson(jsonData: any): Doctor {
        return Object.assign(new Doctor(), jsonData);
    }

}


