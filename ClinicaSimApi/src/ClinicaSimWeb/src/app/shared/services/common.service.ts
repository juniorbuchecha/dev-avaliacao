import { BaseModel } from "../models/base.model";

import { Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { URL_API } from 'src/app/app.Api';


export abstract class CommonService<T extends BaseModel> {

  protected http: HttpClient;
  url = URL_API

  constructor(
    protected apiPath: string,
    protected injector: Injector,
    protected jsonDataToResourceFn: (jsonData: any) => T
  ) {
    this.http = injector.get(HttpClient);
  }

  private getUrl(): string {
    return this.url + this.apiPath;
  }

  getAll(): Observable<T[]> {
    return this.http.get(this.getUrl()).pipe(
      map(this.jsonDataToResources.bind(this)),
      catchError(this.handleError)
    )
  }

  getById(id: any): Observable<T> {
    const url = `${this.getUrl()}/${id}`;

    return this.http.get(url).pipe(
      map(this.jsonDataToResource.bind(this)),
      catchError(this.handleError)
    )
  }

  create(resource: T): Observable<T> {
    return this.http.post(this.getUrl(), resource).pipe(
      map(this.jsonDataToResource.bind(this)),
      catchError(this.handleError)
    )
  }

  update(resource: T): Observable<T> {
    const url = `${this.getUrl()}/${resource.id}`;

    // return this.http.put(url, resource).pipe(
    //   map(() => resource),
    //   catchError(this.handleError)
    // )
    return this.http.put(url, resource).pipe(
      map((valor: any) => valor),
      catchError(this.handleError)
    )

  }

  delete(id: number): Observable<any> {
    const url = `${this.getUrl()}/${id}`;

    return this.http.delete(url).pipe(
      map(() => null),
      catchError(this.handleError)
    )
  }



  // PROTECTED METHODS
  protected jsonDataToResources(jsonData: any[]): T[] {
    const resources: T[] = [];
    jsonData.forEach(
      element => resources.push(this.jsonDataToResourceFn(element))
    );
    return resources;
  }

  protected jsonDataToResource(jsonData: any): T {
    return this.jsonDataToResourceFn(jsonData);
  }

  protected handleError(error: any): Observable<any> {
    console.log("ERRO NA REQUISIÇÃO => ", error);
    return throwError(error);
  }

}