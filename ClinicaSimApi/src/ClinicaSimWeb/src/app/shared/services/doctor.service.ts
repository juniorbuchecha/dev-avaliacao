import { Injectable, Injector } from '@angular/core';
import { CommonService } from "../services/common.service";
import { Doctor } from '../models/doctor.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorService  extends CommonService<Doctor> {

    constructor(protected injector: Injector) {
    super("doctor", injector, Doctor.fromJson);
  }
}
