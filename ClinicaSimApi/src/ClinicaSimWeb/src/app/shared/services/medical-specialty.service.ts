import { Injectable, Injector } from '@angular/core';
import { CommonService } from './common.service';
import { MedicalSpecialty } from '../models/medical-specialty.model';

@Injectable({
  providedIn: 'root'
})
export class MedicalSpecialtyService extends CommonService<MedicalSpecialty> {

  constructor(protected injector: Injector) {
    super("medicalSpecialty", injector, MedicalSpecialty.fromJson);
  }
}
