import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastr: ToastrService) { }


  success(msg: string) {
    this.toastr.success(msg, 'Sucesso');
  }

  error(msg: string) {
    this.toastr.error(msg, 'Algo errado!');
  }
}
