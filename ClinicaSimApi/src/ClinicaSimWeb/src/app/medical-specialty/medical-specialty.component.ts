import { Component, OnInit } from '@angular/core';
import { MedicalSpecialty } from '../shared/models/medical-specialty.model';
import { MedicalSpecialtyService } from '../shared/services/medical-specialty.service';

@Component({
  selector: 'app-medical-specialty',
  templateUrl: './medical-specialty.component.html',
  styleUrls: ['./medical-specialty.component.css']
})
export class MedicalSpecialtyComponent implements OnInit {

  medicalSpecialty: MedicalSpecialty[] = []

  constructor(private _MedicalSpecialtyService: MedicalSpecialtyService) {

    this.medicalSpecialtyAll();
  }

  ngOnInit() {
  }


  medicalSpecialtyAll() {

    this._MedicalSpecialtyService.getAll().subscribe((result: MedicalSpecialty[]) => {
      this.medicalSpecialty = result;
    })

  }


}
