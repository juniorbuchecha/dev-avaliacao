import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { DoctorFormComponent } from './doctors/doctor-form/doctor-form.component';
import { MedicalSpecialtyComponent } from './medical-specialty/medical-specialty.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'doctors', component: DoctorsComponent },
  { path: 'doctors/:id', component: DoctorFormComponent },
  { path: 'medicalSpecialty', component: MedicalSpecialtyComponent },
  { path: '**', component: NotFoundComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
