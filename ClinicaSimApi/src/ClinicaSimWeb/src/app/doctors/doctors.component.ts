import { Component, OnInit } from '@angular/core';
import { Doctor } from '../shared/models/doctor.model';
import { DoctorService } from '../shared/services/doctor.service';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent implements OnInit {

  doctors: Doctor[] = [];
  constructor(private _doctorService: DoctorService) {
    this.getAll();
  }

  ngOnInit() {
  }

  getAll() {
    this._doctorService.getAll().subscribe((doctors: Doctor[]) => {
      this.doctors = doctors;
      //console.log(valor);
    });
  }


}
