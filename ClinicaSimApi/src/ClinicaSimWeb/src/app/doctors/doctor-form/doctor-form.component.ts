import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Doctor } from 'src/app/shared/models/doctor.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DoctorService } from 'src/app/shared/services/doctor.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { MedicalSpecialtyService } from 'src/app/shared/services/medical-specialty.service';
import { MedicalSpecialty } from 'src/app/shared/models/medical-specialty.model';

@Component({
  selector: 'app-doctor-form',
  templateUrl: './doctor-form.component.html',
  styleUrls: ['./doctor-form.component.css']
})
export class DoctorFormComponent implements OnInit {

  formDoctor: FormGroup;
  txtButton: string = 'SALVAR';
  doctor: Doctor;
  medicalSpecialty: MedicalSpecialty[] = [];
  id: string = '0';

  constructor(private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _doctorService: DoctorService,
    private _toastr: ToastService,
    private _MedicalSpecialtyService: MedicalSpecialtyService) {

    debugger;

    this.id = this.route.snapshot.params['id'];
    this.medicalSpecialtyAll();
    this.createDoctor();

    if (!this.isCreate()) {
      this.txtButton = 'ATUALIZAR';
      this.getDoctor(this.id);
    }

  }

  ngOnInit() {

  }

  medicalSpecialtyAll() {

    this._MedicalSpecialtyService.getAll().subscribe((result: MedicalSpecialty[]) => {
      this.medicalSpecialty = result;
    })

  }

  createDoctor() {

    this.formDoctor = this._formBuilder.group({
      cpf: [null, [Validators.required, Validators.maxLength(11), Validators.minLength(11)]],
      name: [null, [Validators.required, Validators.max(255)]],
      medicalSpecialtyId: [1, Validators.required],
      active: [true]
    });
  }

  getDoctor(id: string) {
    this._doctorService.getById(id).subscribe((doctor: Doctor) => {

      this.formDoctor = this._formBuilder.group({
        cpf: [doctor.cpf, [Validators.required, Validators.maxLength(11), Validators.minLength(11)]],
        name: [doctor.name, [Validators.required, Validators.max(255)]],
        id: [doctor.id, [Validators.required]],
        medicalSpecialtyId: [doctor.medicalSpecialtyId, Validators.required],
        active: [doctor.active]
      });

    })
  }

  onSubmit(): void {
    if (this.isCreate()) {
      this.Create();
    } else {
      this.Update();
    }
  }

  private Create(): void {
    this._doctorService.create(this.formDoctor.value).subscribe((result: any) => {
      this.notifications(result);
    })
  }


  private Update(): void {
    this._doctorService.update(this.formDoctor.value).subscribe((result: any) => {
      this.notifications(result);
    })
  }

  isCreate(): boolean {
    if (this.id === '0') {
      return true;
    } else {
      return false;
    }
  }

  notifications(result: any): void {
    if (result.success === true) {
      this._toastr.success("Atualizado com sucesso");
    } else {
      result.data.forEach(element => {
        this._toastr.error(element.message);
      });
    }
  }

}

