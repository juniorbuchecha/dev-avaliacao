﻿using ClinicaSim.Application.Interfaces;
using ClinicaSim.Application.Services;
using ClinicaSim.Domain.Interfaces.Repository;
using ClinicaSim.Infra;
using ClinicaSim.Infra.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClinicaSim.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           
            services.AddEntityFrameworkNpgsql().AddDbContext<ClinicaSimContext>();          
            services.AddMvc();

            services.AddTransient<IDoctorService, DoctorServices>();
            services.AddTransient<IMedicalSpecialtyService, MedicalSpecialtyService>();
            services.AddTransient<IDoctorRepository, DoctorRepository>();

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            //REST CORS*
            app.UseCors(c =>
            {
                c.AllowAnyHeader();
                c.AllowAnyMethod();
                c.AllowAnyOrigin();
            });


            app.UseMvc();          

        }
    }
}
