﻿using ClinicaSim.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace ClinicaSim.API.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : CommonController
    {
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {

            //DoctorServices _serv = new DoctorServices();
            //var lista = _serv.GetAll();
            //return Ok(lista);
            return Ok("ClinicaSim");
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
