﻿using ClinicaSim.Application.Interfaces;
using ClinicaSim.Application.ViewModels;
using ClinicaSim.Application.ViewModels.Doctor;
using Microsoft.AspNetCore.Mvc;

namespace ClinicaSim.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Doctor")]
    public class DoctorController : CommonController
    {
        private readonly IDoctorService _doctorServices;
        public DoctorController(IDoctorService doctorServices)
        {
            _doctorServices = doctorServices;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_doctorServices.GetAll());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(string id)
        {
            return Ok(_doctorServices.Get(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody]DoctorCreateViewModel vm)
        {
            _doctorServices.Create(vm);
            if (_doctorServices.IsValid())
            {
                return Ok(Result("Salvo com sucesso", vm));
            }
            else
            {
                return Ok(Result("Erro ao salvar Médico"));
            }
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Put(string id, [FromBody]DoctorCreateViewModel vm)
        {
            _doctorServices.Update(vm);
            if (_doctorServices.IsValid())
            {
                var result = Result("Atualizado com sucesso", vm);
                return Ok(result);
            }
            else
            {
                return Ok(Result("Erro ao Atualizar Médico"));
            }
        }

        public ResultViewModel Result(string message = "", object obj = null)
        {
            if (_doctorServices.IsValid())
            {
                return new ResultViewModel
                {
                    Success = true,
                    Message = message,
                    Data = obj
                };
            }
            else
            {
                return new ResultViewModel
                {
                    Success = false,
                    Message = message,
                    Data = _doctorServices.ErrorsList()
                };
            }
        }

    }
}