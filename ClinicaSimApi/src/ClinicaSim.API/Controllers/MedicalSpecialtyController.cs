﻿using ClinicaSim.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ClinicaSim.API.Controllers
{
    [Produces("application/json")]
    [Route("api/MedicalSpecialty")]
    public class MedicalSpecialtyController : CommonController
    {
        private readonly IMedicalSpecialtyService _medicalSpecialtyService;

        public MedicalSpecialtyController(IMedicalSpecialtyService medicalSpecialtyService)
        {
            _medicalSpecialtyService = medicalSpecialtyService;
        }

        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            var lista = _medicalSpecialtyService.GetAll();
            return Ok(lista);
        }
    }
}