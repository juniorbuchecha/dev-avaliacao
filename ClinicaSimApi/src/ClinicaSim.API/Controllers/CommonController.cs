﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace ClinicaSim.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Common")]
    public class CommonController : Controller
    {
        protected List<string> errorList = new List<string>();

        protected new IActionResult Response(object retorno = null)
        {
            if (!ExistError())
            {
                return Ok(retorno);
            }
            return BadRequest(new
            {
                erro = errorList
            });
        }

        protected void ModelStateError()
        {
            var erros = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in erros)
            {
                errorList.Add(item.ErrorMessage);
            }
        }

        public void SetError(string erro)
        {
            errorList.Add(erro);
        }

        public bool ExistError()
        {
            ModelStateError();
            return errorList.Count > 0;
        }
    }
}