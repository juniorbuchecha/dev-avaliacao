﻿using ClinicaSim.Domain.Entities;
using ClinicaSim.Domain.ReadOnly;
using System.Collections.Generic;

namespace ClinicaSim.Domain.Interfaces.Repository
{
    public interface IDoctorRepository : IRepository<Doctor>
    {
        IEnumerable<DoctorReadOnly> GetAllDoctorSpecialty();
        Doctor GetCPF(string CPF);
    }
}
