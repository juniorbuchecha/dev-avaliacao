﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        TEntity Create(TEntity obj);
        TEntity Get(Guid id);
        IEnumerable<TEntity> GetAll();
        TEntity Update(TEntity obj);
        void Delete(TEntity obj);
        int SaveChanges();
    }
}
