﻿using ClinicaSim.Domain.Entities;

namespace ClinicaSim.Domain.Interfaces.Repository
{
    public interface IMedicalSpecialtyRepository : IRepository<MedicalSpecialty>
    {
    }
}
