﻿using System;
namespace ClinicaSim.Domain.ReadOnly
{
    public class DoctorReadOnly
    {
        public Guid Id { get; set; }
        public string CPF { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int MedicalSpecialtyId { get; set; }
        public string MedicalSpecialty { get; set; }


        public DoctorReadOnly()
        {

        }
    }
}
