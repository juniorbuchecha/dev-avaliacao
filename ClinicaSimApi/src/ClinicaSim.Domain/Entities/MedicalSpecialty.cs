﻿using System.Collections.Generic;

namespace ClinicaSim.Domain.Entities
{
    public class MedicalSpecialty
    {
        public int Id { get; private set; }
        public string Description { get; private set; }

        public virtual ICollection<Doctor> Doctors { get; set; }

        protected MedicalSpecialty()
        {
        }

        public MedicalSpecialty(int id, string description)
        {
            Id = id;
            Description = description;
        }

    }
}
