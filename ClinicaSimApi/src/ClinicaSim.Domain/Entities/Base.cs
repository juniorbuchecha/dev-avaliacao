﻿using FluentValidator;
using System;

namespace ClinicaSim.Domain.Entities
{
    public class Base : Notifiable
    {
        public Guid Id { get; private set; }

        public Base()
        {
            Id = Guid.NewGuid();
        }
    }
}
