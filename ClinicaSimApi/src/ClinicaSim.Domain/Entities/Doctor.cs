﻿using ClinicaSim.Domain.Validation;
using FluentValidator.Validation;
using System;

namespace ClinicaSim.Domain.Entities
{
    public class Doctor : Base
    {
        public string CPF { get; private set; }
        public string Name { get; private set; }
        public bool Active { get; private set; }
        public int MedicalSpecialtyId { get; private set; }
        public DateTime DateCreate { get; private set; }

        public virtual MedicalSpecialty MedicalSpecialty { get; set; }


        protected Doctor()
        {

        }

        public Doctor(string cpf, string name, int medicalSpecialtyId)
        {
            CPF = cpf;
            Name = name;
            MedicalSpecialtyId = medicalSpecialtyId;

            DateCreate = DateTime.Now;
            Active = true;

            AddNotifications(new ValidationContract()
            .Requires()
            .HasMaxLen(Name, 255, "Nome", "O nome deve conter no maximo 255 caracteres")
            .HasMinLen(CPF, 11, "CPFCNPJ", "O nome deve conter pelo menos 14 caracteres")
            .IsTrue(CPFValidation.Validar(CPF), "CPF", "CPF Inválido")
           );
        }

        public void Update(string cpf, string name, int medicalSpecialtyId, bool active = true)
        {
            CPF = cpf;
            Name = name;
            MedicalSpecialtyId = medicalSpecialtyId;
            Active = active;

            AddNotifications(new ValidationContract()
            .Requires()
            .HasMaxLen(Name, 255, "Nome", "O nome deve conter no maximo 255 caracteres")
            .HasMinLen(CPF, 11, "CPFCNPJ", "O nome deve conter pelo menos 14 caracteres")
            .IsTrue(CPFValidation.Validar(CPF), "CPF", "CPF Inválido")
           );
        }


        public override string ToString()
        {
            return $"{this.Name}, CPF: {this.CPF}";
        }


    }
}
