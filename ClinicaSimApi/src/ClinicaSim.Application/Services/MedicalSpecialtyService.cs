﻿using ClinicaSim.Application.Interfaces;
using ClinicaSim.Domain.Entities;
using ClinicaSim.Infra.Repository;
using System.Collections.Generic;
using System.Linq;

namespace ClinicaSim.Application.Services
{
    public class MedicalSpecialtyService : CommonService, IMedicalSpecialtyService
    {
        public readonly MedicalSpecialtyRepository _rep;

        public MedicalSpecialtyService()
        {
            _rep = new MedicalSpecialtyRepository();
        }

        public IEnumerable<MedicalSpecialty> GetAll()
        {

            var lista = _rep.GetAll().ToList();
            //AJUSTE CASO NÃO DE TEMPO PARA FAZER O CADASTRO DAS ESPECILIDADES
            if (lista.Count == 0)
            {
                Seed();
                return _rep.GetAll().ToList();
            }

            return lista;
        }

        public void Seed()
        {
            var vm1 = new MedicalSpecialty(1, "Otorrino");
            var vm2 = new MedicalSpecialty(2, "Pediatra");
            var vm3 = new MedicalSpecialty(3, "Cardiologia");
            var vm4 = new MedicalSpecialty(3, "Psiquiatria");
            _rep.Create(vm1);
            _rep.Create(vm2);
            _rep.Create(vm3);
            _rep.Create(vm4);

            Save();
        }

        private bool Save()
        {
            return _rep.SaveChanges() > 0;
        }

    }
}
