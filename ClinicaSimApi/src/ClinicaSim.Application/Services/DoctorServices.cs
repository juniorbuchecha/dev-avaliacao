﻿using ClinicaSim.Application.Interfaces;
using ClinicaSim.Application.ViewModels.Doctor;
using ClinicaSim.Domain.Entities;
using ClinicaSim.Domain.Interfaces.Repository;
using ClinicaSim.Domain.ReadOnly;
using ClinicaSim.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClinicaSim.Application.Services
{
    public class DoctorServices : CommonService, IDoctorService
    {
        private readonly IDoctorRepository _rep;
        protected bool isValid = true;

        public DoctorServices(IDoctorRepository rep)
        {
            _rep = rep;
        }

        public void Create(DoctorCreateViewModel vm)
        {
            var doctor = new Doctor(vm.CPF, vm.Name, vm.MedicalSpecialtyId);
            if (_rep.GetCPF(vm.CPF) != null)
            {
                doctor.AddNotification("CPF", "CPF já cadastrado");
            }

            if (doctor.Valid)
            {
                vm.Id = doctor.Id.ToString();
                _rep.Create(doctor);
                _rep.SaveChanges();
            }
            else
            {
                foreach (var item in doctor.Notifications)
                {
                    AddErrorDomain(item);
                }
            }
        }

        public void Update(DoctorCreateViewModel vm)
        {
            var doctor = _rep.Get(Guid.Parse(vm.Id));
            doctor.Update(vm.CPF, vm.Name, vm.MedicalSpecialtyId, vm.Active);

            var cpfdoctor = _rep.GetCPF(vm.CPF);

            if (cpfdoctor != null)
            {
                if (cpfdoctor.Id != Guid.Parse(vm.Id))
                {
                    doctor.AddNotification("CPF", "Esse CPF pertecem a outro Médico, não foi possivel a atualização");
                }
            }


            if (doctor.Valid)
            {
                _rep.Update(doctor);
                Save();
            }
            else
            {
                foreach (var item in doctor.Notifications)
                {
                    AddErrorDomain(item);
                }
            }

        }

        public IList<DoctorReadOnly> GetAll()
        {
            var doctors = _rep.GetAllDoctorSpecialty().ToList();
            return doctors;
        }

        public Doctor Get(string Id)
        {
            return _rep.Get(Guid.Parse(Id));
        }

        private bool Save()
        {
            return _rep.SaveChanges() > 0;
        }


    }
}
