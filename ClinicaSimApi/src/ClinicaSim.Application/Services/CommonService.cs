﻿using ClinicaSim.Application.Interfaces;
using FluentValidator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClinicaSim.Application.Services
{
    public class CommonService : Notifiable, ICommonService
    {
        public CommonService()
        {

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void AddErrorDomain(Notification notification)
        {
            AddNotification(notification);
        }

        public bool IsValid()
        {
            return Notifications.ToList().Count() == 0;
        }

        public ICollection<Notification> ErrorsList()
        {
            return Notifications.ToList();
        }


    }
}
