﻿using ClinicaSim.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Application.Interfaces
{
    public interface IMedicalSpecialtyService
    {
        IEnumerable<MedicalSpecialty> GetAll();
    }
}
