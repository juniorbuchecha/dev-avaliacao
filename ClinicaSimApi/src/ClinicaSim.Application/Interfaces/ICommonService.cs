﻿using FluentValidator;
using System.Collections.Generic;

namespace ClinicaSim.Application.Interfaces
{
    public interface ICommonService
    {
        void AddErrorDomain(Notification notification);
        bool IsValid();
        ICollection<Notification> ErrorsList();
    }
}
