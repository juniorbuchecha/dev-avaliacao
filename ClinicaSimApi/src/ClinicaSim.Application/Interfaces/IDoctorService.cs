﻿using ClinicaSim.Application.ViewModels.Doctor;
using ClinicaSim.Domain.Entities;
using ClinicaSim.Domain.ReadOnly;
using System.Collections.Generic;

namespace ClinicaSim.Application.Interfaces
{
    public interface IDoctorService : ICommonService
    {
        void Create(DoctorCreateViewModel vm);
        void Update(DoctorCreateViewModel vm);
        IList<DoctorReadOnly> GetAll();
        Doctor Get(string Id);
        
    }
}
