﻿namespace ClinicaSim.Application.ViewModels.Doctor
{
    public class DoctorCreateViewModel
    {
        public string Id { get; set; }
        public string CPF { get; set; }
        public string Name { get; set; }
        public int MedicalSpecialtyId { get; set; }
        public bool Active { get; set; }

        public DoctorCreateViewModel()
        {

        }
    }
}
